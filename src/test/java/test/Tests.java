package test;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Flaky;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;

@Epic("Allure report for allo.ua")
public class Tests {

    private String mainUrl = "https://allo.ua/";
    private String notExistUserEmail = "NotExistUserEmail@test.su";
    private String password = "12345678";
    private final String VALIDATION_ERROR = "Це поле є обов'язковим.";
    private final String NOT_EXIST_USER_ERROR = "Користувач з email NotExistUserEmail@test.su не зареєстрований. Будь ласка, зареєструйтесь чи авторизуйтесь за допомогою номеру телефона.";

    @BeforeSuite
    public void openSite() {

        open(mainUrl);
        String url = url();
        Assert.assertEquals(url, mainUrl);
    }

    @Test(priority = 1)
    @Description("check that user is able to open log in popup")
    public void openAuthPopUp() {

        $(By.xpath("//*[@id=\"customer-header-menu\"]")).click();
        $(By.xpath("//*[@id=\"customer-popup-menu\"]")).should(Condition.appears);
    }

    @Test(priority = 2)
    @Description("Check that user see the alerts when empty form submited")
    public void LogInWithEmptyFields() throws InterruptedException {

        $(By.xpath("//*[@id=\"customer-login-menu\"]/div[1]/div[1]/form/div/button")).click();
        Thread.sleep(1500);
        Assert.assertEquals($(By.xpath("//*[@id=\"customer-login-menu\"]/div[1]/div[1]/form/div/div[1]/div[1]/div/span")).getText(), VALIDATION_ERROR);
        Assert.assertEquals($(By.xpath("//*[@id=\"customer-login-menu\"]/div[1]/div[1]/form/div/div[1]/div[2]/div/span")).getText(), VALIDATION_ERROR);
    }

    @Test(priority = 3)
    @Description("Check that user is not able to log in with wrong email")
    public void LoginWithNotExistEmail() throws InterruptedException {

        $(By.xpath("//*[@id=\"auth\"]")).setValue(notExistUserEmail);
        $(By.xpath("//*[@id=\"v-login-password\"]")).setValue(password);
        $(By.cssSelector("#customer-login-menu > div.login-and-register > div:nth-child(1) > form > div > button")).click();
        Thread.sleep(1500);
        String error = $(By.xpath("//*[@id=\"customer-login-menu\"]/div[1]/div[1]/form/div/div[1]/div[1]/div/span")).shouldBe(Condition.visible).getText();
        Assert.assertEquals(error, NOT_EXIST_USER_ERROR);
    }

    @Test(priority = 4)
    @Description("Check that user is able to open register tab")
    public void openRegisterTab() throws InterruptedException {

        $(By.xpath("//*[@id=\"customer-popup-menu\"]/div[1]/ul/li[2]")).click();
        Thread.sleep(1500);
        Assert.assertEquals($(By.xpath("//*[@id=\"form-validate-register\"]/button")).getText(), "Зареєструватися");
    }

    @Flaky
    @Test(priority = 5)
    @Description("Check that user is not able to submit form with empty fields")
    public void submitEmptyRegisterForm() {
        $(By.xpath("//*[@id=\"form-validate-register\"]/button")).click();
        //Thread.sleep(1500);
        Assert.assertEquals($(By.xpath("//*[@id=\"form-validate-register\"]/div/div[1]/div/span")).getText(), VALIDATION_ERROR);
        Assert.assertEquals($(By.xpath("//*[@id=\"form-validate-register\"]/div/div[2]/div/span")).getText(), VALIDATION_ERROR);
        Assert.assertEquals($(By.xpath("//*[@id=\"form-validate-register\"]/div/div[4]/div/span")).getText(), VALIDATION_ERROR);
    }

    @AfterSuite
    public void close() {
        Selenide.close();
    }

}
